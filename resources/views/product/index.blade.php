@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Products</div>
				<div class="panel-body">
				<style type="text/css">th,td{ padding: 20px;} </style>
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<a style="float:right;" href="{{ url('/product/create') }}">Create New Product</a>
					<table cellpadding="25">
						@if (count($products) > 0)
							<tr >
								<th>Product Name</th><th>Product Price</th><th>Options</th>
							</tr>
							@foreach ($products as $product)
								<tr><td>{{ $product->title }}</td> <td>{{ $product->price }}</td>
								<td>
									<a href="{{ url('product/view/'.$product->id) }}">View</a>|
									<a href="{{ url('product/edit/'.$product->id) }}">Edit</a>|
									<a href="{{ url('product/delete/'.$product->id) }}" onclick="return confirm('Are you sure?')">Delete</a>
								</td>
							</tr>
							@endforeach	
						@else
						<tr >
							<td>No record found</td>
						</tr>
						@endif		
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
@endsection