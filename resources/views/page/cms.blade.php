<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ $title }}</title>

	<style type="text/css">
		{{ $css }}
	</style>
	
	<script type="text/javascript">
	{{ $js }}
	</script>

</head>
<body>
<?php  echo $body; ?>
</body>
</html>
