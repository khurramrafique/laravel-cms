@extends('app') 

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Create Page</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('page/store') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="form-group">
							<label class="col-md-4 control-label">Page Title</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="title" value="{{ old('title') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Page Nav Title</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="nav_title" value="{{ old('nav_title') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Page Slug</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="slug" value="{{ old('slug') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Page Body</label>
							<div class="col-md-6">
								<textarea id="body" name="body" rows="7" class="form-control ckeditor"></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Page CSS</label>
							<div class="col-md-6">
								<textarea name="css" rows="5" class="form-control" value="{{ old('css') }}" ></textarea>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Page JS</label>
							<div class="col-md-6">
								<textarea name="js" rows="5" class="form-control" value="{{ old('js') }}" ></textarea>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Show Title</label>
							<div class="col-md-6 ">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="show_title" value="1">
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Show in Nav</label>
							<div class="col-md-6 ">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="show_in_nav" value="1">
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Create
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection