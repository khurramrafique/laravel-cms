@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Users</div>
				<div class="panel-body">
				<style type="text/css">th,td{ padding: 20px;} </style>
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<a style="float:right;" href="{{ url('/user/create') }}">Create New User</a>
					
					<table cellpadding="25">
						@if (count($users) > 0)
							<tr >
								<th>Name</th><th>Email</th><th>Options</th>
							</tr>
							@foreach ($users as $user)
								<tr><td>{{ $user->first_name }} {{ $user->last_name }}</td><td>{{ $user->email }}</td>
								<td>
									<a href="{{ url('user/view/'.$user->id) }}">View</a>|
									<a href="{{ url('user/update/'.$user->id) }}">Edit</a>|
									<a href="{{ url('user/delete/'.$user->id) }}" onclick="return confirm('Are you sure?')" >Delete</a>
								</td>
							</tr>
							@endforeach
						@else
						<tr >
							<td>No record found</td>
						</tr>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
