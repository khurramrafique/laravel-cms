@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Dashboard</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif			
					<ul>
						<li><a href="{{ url('/users') }}">Users</a></li>
						<li><a href="{{ url('/pages') }}">Pages</a></li>
						<li><a href="{{ url('/products') }}">Products</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
