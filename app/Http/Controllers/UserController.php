<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function dashboard()
    {   
        return view('user.dashboard');
    }

    public function users()
    {   
        $users = User::where('role', 2)->paginate(10);
        return view('user.index', ['users' => $users]);
    }

    public function view($id)
    {   
        $user = User::find($id);
        return response()->view('user.view',$user);
    }

    public function update(Request $request,$id)
    {   
        $User = User::find($id);
        if ($request->isMethod('post')) 
        { 
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email|required'
        ]);

        if (!$validator->fails()) 
        {
        $User->first_name = $request->input('first_name');
        $User->last_name = $request->input('last_name');
        $User->email = $request->input('email');
        $User->save();
        return redirect()->back()->with('status', 'Profile updated successfully!');
        }
        else
        {
            return redirect('profile')
            ->withErrors([
                $validator->errors()->all()
            ]);    
        }
        }
        return response()->view('user.edit',$User);
    }

    public function create(Request $request)
    {   
        if ($request->isMethod('post')) 
        {  
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'password' => 'required',
                //'roles' => 'required',
                'email' => 'required|email|max:255|unique:users'
            ]);

            if (!$validator->fails()) 
            {   
               //echo "<pre>"; print_r($request->input('roles')); exit;
                $user = User::create([
                    'first_name' => $request->input('first_name'),
                    'last_name' => $request->input('last_name'),
                    'email' => $request->input('email'),
                    'password' => bcrypt($request->input('password')),
                    'profile_pic' => 'avatar.jpg',
                    'role' => 2,
                    'permissions' => 'permissions',
                ]);

                if($user)
                return redirect('users')->with('status', 'User has been created successfully!');
                else
                return redirect('user/create')->with('errors', 'Unable to create user!'); 
            }
            else
            {
                return redirect('user/create')
                ->withErrors([
                    $validator->errors()->all()
                ]);    
            }
        }
        return response()->view('user.create');
    }

    public function delete($id)
    {   
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('status', 'User deleted successfully!');
    }
}