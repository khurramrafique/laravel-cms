<?php 

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;

class ProfileController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('role');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{   
		return view('home');
	}


	/**
     * Update the user's profile.
     *
     * @param  Request  $request
     * @return Response
     */
    public function profile(Request $request)
    {
        if ($request->user()) {           
            return response()->view('auth.profile', $request->user());
        }
    }


    public function update(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'email|required'
        ]);

        if (!$validator->fails()) 
        {
        $User = User::find($request->user()->id);
        $User->first_name = $request->input('first_name');
        $User->last_name = $request->input('last_name');
        $User->email = $request->input('email');
        $User->save();
        return redirect()->back()->with('status', 'Profile updated successfully!');
        }
        else
        {
            return redirect('profile')
            ->withErrors([
                $validator->errors()->all()
            ]);    
        }  
    }


}
