<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Product;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $products = Product::all();
        return response()->view('product.index',['products'=>$products]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function user_products(Request $request)
    {   
        $user_id = $request->user()->id;
        $products = Product::all()->where('user',$user_id);
        return response()->view('product.index',['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('product.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'price' => 'required|integer',    
        ]);

        if (!$validator->fails()) 
        {
        $Product = Product::create([
            'title' => $request->input('title'),
            'price' => $request->input('price'),
            'user' => $request->user()->id,
            'status' => 1,
        ]);

        return redirect()->back()->with('status', 'Product created successfully!');
        }
        else
        {
            return redirect('product/create')
            ->withErrors([
                $validator->errors()->all()
            ]);    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return response()->view('product.view',$product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return response()->view('product.edit',$product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {   
        if ($request->isMethod('post')) 
        { 
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'price' => 'required|integer',
            ]);
            if (!$validator->fails()) 
            {
           $update_product = Product::where('id', $id)
                ->update([
                'title' => $request->input('title'),
                'price' => $request->input('price'),
            ]);

            return redirect()->back()->with('status', 'Product Updated successfully!');
            }
            else
            {
                return redirect('product/edit/'.$id)
                ->withErrors([
                    $validator->errors()->all()
                ]);    
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {   
        $product = Product::find($id);
        $product->delete();
        return redirect()->back()->with('status', 'Product has been deleted successfully!');
    }
}