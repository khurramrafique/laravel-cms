<?php 

namespace App\Http\Controllers;

use App\Page;
use Validator;
use Illuminate\Http\Request;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{   
		//echo "Home Page";
        //return view('home');
	}


    public function page(Request $request,$id)
    {   
        $page = Page::where('slug', $id)->first();
        return response()->view('page.cms', $page);   
    }


}
