<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Page;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{   
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $pages = Page::all();
        return response()->view('page.index',['pages'=>$pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {   
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'nav_title' => 'required',
            'slug' => 'required',
            'body' => 'required',
            'css' => 'required',
            'js' => 'required',
        ]);

        if (!$validator->fails()) 
        {
        $Page = Page::create([
            'title' => $request->input('title'),
            'nav_title' => $request->input('nav_title'),
            'slug' => $request->input('slug'),
            'body' => $request->input('body'),
            'css' => $request->input('css'),
            'js' => $request->input('js'),
            'show_title' => $request->input('show_title'),
            'show_in_nav' => $request->input('show_in_nav'),
        ]);

        return redirect()->back()->with('status', 'Page created successfully!');
        }
        else
        {
            return redirect('page/create')
            ->withErrors([
                $validator->errors()->all()
            ]);    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $page = Page::find($id);
        return response()->view('page.view',$page);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $page = Page::find($id);
        return response()->view('page.edit',$page);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {   
        if ($request->isMethod('post')) 
        { 
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'nav_title' => 'required',
                'slug' => 'required',
                'body' => 'required',
                'css' => 'required',
                'js' => 'required',
            ]);
            if (!$validator->fails()) 
            {
           $update_page = Page::where('id', $id)
                ->update([
                'title' => $request->input('title'),
                'nav_title' => $request->input('nav_title'),
                'slug' => $request->input('slug'),
                'body' => $request->input('body'),
                'css' => $request->input('css'),
                'js' => $request->input('js'),
                'show_title' => $request->input('show_title'),
                'show_in_nav' => $request->input('show_in_nav'),
            ]);

            return redirect()->back()->with('status', 'Page Updated successfully!');
            }
            else
            {
                return redirect('page/edit/'.$id)
                ->withErrors([
                    $validator->errors()->all()
                ]);    
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {   
        $page = Page::find($id);
        $page->delete();
        return redirect()->back()->with('status', 'Page has been deleted successfully!');
    }
}
