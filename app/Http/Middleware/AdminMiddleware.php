<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
    {   
        if ($request->user()) {
            if ($request->user()->role !=1) {
                return redirect('profile')->with('status', 'Sorry. you are unauthorized to access this'); 
            }
        }
        
        return $next($request);
    }
}
