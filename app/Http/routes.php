<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', function () {
    return view('welcome');
});

Route::get('cms/{id}', 'HomeController@page');

Route::get('profile', 'ProfileController@profile');

Route::post('profile/update', 'ProfileController@update');


// Users routes...

Route::any('admin', 'UserController@dashboard');

Route::any('user/create', 'UserController@create');

Route::get('users', 'UserController@users');

Route::get('user/view/{id}', 'UserController@view');

Route::any('user/update/{id}', 'UserController@update');

Route::get('user/delete/{id}', 'UserController@delete');

Route::get('user/suspend/{id}', 'UserController@suspend');


// Pages routes...
Route::get('pages', 'PageController@index');

Route::get('page/create', 'PageController@create');

Route::post('page/store', 'PageController@store');

Route::get('page/view/{id}', 'PageController@show');

Route::get('page/edit/{id}', 'PageController@edit');

Route::post('page/update/{id}', 'PageController@update');

Route::get('page/delete/{id}', 'PageController@destroy');


// Products routes...
Route::get('products', [
    'middleware' => 'admin',
    'uses' => 'ProductController@index'
]);

Route::get('user_products', 'ProductController@user_products');

Route::get('product/create', 'ProductController@create');

Route::post('product/store', 'ProductController@store');

Route::get('product/view/{id}', 'ProductController@show');

Route::get('product/edit/{id}', 'ProductController@edit');

Route::post('product/update/{id}', 'ProductController@update');

Route::get('product/delete/{id}', 'ProductController@destroy');


// Authentication routes...
Route::get('login', 'Auth\AuthController@getLogin');

Route::post('login', 'Auth\AuthController@postLogin');

Route::get('logout', 'Auth\AuthController@getLogout');

Route::get('register', 'Auth\AuthController@getRegister');

Route::post('register', 'Auth\AuthController@postRegister');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);